<?php

namespace Drupal\kala_migrate\Plugin\migrate\destination;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\file\Entity\File;
use Drupal\file\Plugin\Field\FieldType\FileItem;
use Drupal\media\Entity\Media;

/**
 * Provide a trait for destination entities with media fields.
 *
 * It only should be used on destinations that extends
 * Drupal\migrate\Plugin\migrate\destination\EntityContentBase class and use one
 * of the Media<MediaType>ImportFromFile process plugin from this module.
 */
trait ContentWithMediaDestinationTrait {

  /**
   * The entity media fields.
   *
   * @var array
   */
  private array $mediaFields = [];

  /**
   * Media entity's media field.
   *
   * @var array
   */
  private string $internalMediaField = 'field_media_image';

  /**
   * Set the entity media fields.
   *
   * @param array $media_fields
   *   The media field names.
   */
  public function setMediaFields(array $media_fields) {
    $this->mediaFields = $media_fields;
  }

  /**
   * Get the media fields.
   *
   * @return array
   *   The media field names array.
   */
  public function getMediaFields(): array {
    return $this->mediaFields;
  }

  /**
   * Set the media entity media field.
   *
   * @param string $internal_media_field
   *   The media field name.
   */
  public function setInternalMediaField($internal_media_field) {
    $this->internalMediaField = $internal_media_field;
  }

  /**
   * Get the media entity media field.
   *
   * @return string
   *   The media field name.
   */
  public function getInternalMediaField() {
    return $this->internalMediaField;
  }

  /**
   * Delete the specified destination object from the target Drupal.
   *
   * @param array $destination_identifier
   *   The ID of the destination object to delete.
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function rollback(array $destination_identifier) {
    // Attempt to remove the media.
    if (count($this->mediaFields)) {
      $entity = $this->storage->load(reset($destination_identifier));
      if ($entity instanceof ContentEntityInterface) {
        foreach ($this->mediaFields as $field_name) {
          $media = $entity->$field_name->entity;
          if ($media instanceof Media) {
            $media_items = $media->get($this->internalMediaField);
            foreach ($media_items as $media_item) {
              if ($media_item instanceof FileItem) {
                $fid = $media_item->target_id;
                $file = File::load($fid);
                if ($file instanceof File) {
                  $file->delete();
                }
              }
            }
            $media->delete();
          }
        }
      }
    }

    parent::rollback($destination_identifier);
  }

}
