<?php

namespace Drupal\kala_migrate\Plugin\migrate\process;

use Drupal\migrate\MigrateException;
use Drupal\migrate\MigrateExecutableInterface;
use Drupal\migrate\ProcessPluginBase;
use Drupal\migrate\Row;

/**
 * Perform FileUrl transformations.
 *
 * This plugin should be used to get the file URL from an external source.
 * - source: The field name on the D7 website that stores the file.
 * - base_url: The URL of the file folder. Note that usually it's under
 * "/sites/default/files/". Do not use the URL that stores the files for a given
 * field, but the site files URL, use the '/' at the end.
 *
 * Example:
 *
 * @code
 * destination:
 *   # assuming we're using a plugin that has a media image file.
 *   plugin: entity_with_media:node
 * source:
 *   # assuming we're using a source that has an image field.
 * process:
 *   title: title
 *   uid:
 *     plugin: default_value: 1
 *   nid:
 *     -
 *       plugin: get
 *       source: nid
 *   field_media_image_url:
 *     plugin: file_url
 *     source: field_with_image_file
 *     base_url: 'https://www.mydrupal7-website.com/sites/default/files/'
 *
 *
 * @endcode
 *
 * @MigrateProcessPlugin(
 *   id = "file_url"
 * )
 */
class FileUrl extends ProcessPluginBase {

  use SourceDatabaseTrait;

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, array $plugin_definition) {
    $configuration += [
      'base_url' => NULL,
    ];
    parent::__construct($configuration, $plugin_id, $plugin_definition);
  }

  /**
   * {@inheritdoc}
   */
  public function transform($value, MigrateExecutableInterface $migrate_executable, Row $row, $destination_property) {
    $database = $this->getSourceDatabase($migrate_executable);

    $file_uri = $database->query('SELECT uri FROM {file_managed} WHERE fid = :fid', [
      ':fid' => $value['fid'],
    ])->fetchCol();

    if (count($file_uri)) {
      $result = str_replace('public://', $this->configuration['base_url'], $file_uri[0]);
      $result = str_replace('private://', $this->configuration['base_url'], $result);

      return $result;
    }
    else {
      throw new MigrateException(sprintf('The %s fid does not exist.', $value['fid']));
    }
  }

}
