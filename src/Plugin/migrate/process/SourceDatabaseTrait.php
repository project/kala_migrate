<?php

namespace Drupal\kala_migrate\Plugin\migrate\process;

use Drupal\migrate\MigrateExecutableInterface;

/**
 * Provides a trait for getting the source database connection.
 *
 * @package Drupal\newday_migration\Plugin\migrate\process
 */
trait SourceDatabaseTrait {

  /**
   * Return the source database connection.
   *
   * @param \Drupal\migrate\MigrateExecutableInterface $migrate_executable
   *   The Migrate Executable.
   *
   * @return \Drupal\Core\Database\Connection
   *   The source database connection
   *
   * @throws \ReflectionException
   */
  public function getSourceDatabase(MigrateExecutableInterface $migrate_executable) {
    $reflection = new \ReflectionClass($migrate_executable);
    $property = $reflection->getProperty('source');
    $property->setAccessible(TRUE);
    $source = $property->getValue($migrate_executable);
    /** @var \Drupal\Core\Database\Connection $database */
    $database = $source->getDatabase();

    return $database;
  }

}
