<?php

namespace Drupal\kala_migrate\Plugin\migrate\process;

use Drupal\Core\File\FileSystemInterface;
use Drupal\media\Entity\Media;
use Drupal\migrate\MigrateExecutableInterface;
use Drupal\migrate\Row;
use Drupal\migrate_file\Plugin\migrate\process\ImageImport;

/**
 * Imports import media image from external file source.
 *
 * Extends image_import plugin to create a media entity during the image import.
 * - source: The file URL. Use with file_url plugin.
 * - destination: The folder to save the media image.
 *
 * @see \Drupal\migrate_file\Plugin\migrate\process\ImageImport
 *
 * Example:
 *
 * @code
 * destination:
 *   # assuming we're using a plugin that has a media image file.
 *   plugin: entity_with_media:node
 * source:
 *   # assuming we're using a source that has an image field.
 *   constants:
 *     media_destination: 'public://media-destination-folder/'
 * process:
 *   title: title
 *
 *   uid:
 *     plugin: default_value: 1
 *
 *   nid:
 *     -
 *       plugin: get
 *       source: nid
 *
 *   field_media_image_url:
 *     plugin: file_url
 *     source: field_with_image_file
 *     base_url: 'https://www.mydrupal7-website.com/sites/default/files/'
 *
 *   field_media_image:
 *     -
 *       plugin: media_image_import_from_file
 *       source: '@field_media_image_url'
 *       destination: 'constants/media_destination'
 *       title: field_file_image/0/title
 *       alt: field_file_image/0/alt
 *       height: field_file_image/0/height
 *       width: field_file_image/0/width
 *       skip_on_error: true
 *
 * @endcode
 *
 * @MigrateProcessPlugin(
 *   id = "media_image_import_from_file"
 * )
 */
class MediaImageImportFromFile extends ImageImport {

  /**
   * {@inheritdoc}
   */
  protected function getOverwriteMode() {
    return FileSystemInterface::EXISTS_ERROR;
  }

  /**
   * {@inheritdoc}
   */
  public function transform($value, MigrateExecutableInterface $migrate_executable, Row $row, $destination_property) {
    // Set time limit to 5 min in order to have time to download large files.
    set_time_limit(300);
    $transformed_value = parent::transform($value, $migrate_executable, $row, $destination_property);
    $fid = $transformed_value['target_id'] ?? NULL;
    $media_field = $this->configuration['media_field'] ?? 'field_media_image';
    $media_bundle = $this->configuration['media_bundle'] ?? 'image';

    // Check if media already exists.
    $media = NULL;
    if (is_numeric($fid)) {
      $result = \Drupal::getContainer()->get('entity_type.manager')
        ->getStorage('media')
        ->loadByProperties([
          $media_field => $fid,
          'bundle' => $media_bundle,
        ]);
      if (count($result)) {
        $media = reset($result);
      }
    }
    else {
      return NULL;
    }

    if (!$media instanceof Media) {
      // Create a new media based on the image.
      $media = Media::create(['bundle' => $media_bundle]);
    }

    $arr = explode('/', $value);
    $filename = end($arr);
    $media->set($media_field, [
      [
        'target_id' => $fid,
        'title' => isset($transformed_value['title']) && strlen($transformed_value['title']) ? $transformed_value['title'] : $filename,
        'alt' => isset($transformed_value['alt']) && strlen($transformed_value['alt']) ? $transformed_value['alt'] : $filename,
        'height' => $transformed_value['height'],
        'width' => $transformed_value['width'],
      ],
    ]);

    $media->save();

    return ['target_id' => $media->id()];
  }

}
